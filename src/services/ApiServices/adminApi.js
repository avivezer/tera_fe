import {authenticationService} from "../AuthenticationService";
import axios from "axios";
import Config from "../../config/Config";

const adminApi = {

    AddNewSite: async (site) => {
        if (!authenticationService.currentUserValue)
            return;
        let SystemAdmin = authenticationService.currentUserValue;

            let requestconfig = {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': authenticationService.currentUserValue.token,
                },
            };
            return (await axios.post(
                `${Config.globalUrl}/api/v1/webapp/eyeSwiftAdmins/${SystemAdmin.profile.UserID}/site/signup`,
                site,
                requestconfig
            ));
    },
    EditSite: async (site_data,SiteID) => {
        if (!authenticationService.currentUserValue)
            return;
        let SystemAdmin = authenticationService.currentUserValue;

            let requestconfig = {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': authenticationService.currentUserValue.token,
                },
            };
            return (await axios.post(
                `${Config.globalUrl}/api/v1/webapp/eyeSwiftAdmins/${SystemAdmin.profile.UserID}/sites/${SiteID}/profile`,
                site_data,
                requestconfig
            ));
    },
    getAllSites: async () => {
        if (!authenticationService.currentUserValue)
            return;
        let SystemAdmin = authenticationService.currentUserValue;

            let requestconfig = {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': authenticationService.currentUserValue.token,
                },
            };
            return await axios.get(
                `${Config.globalUrl}/api/v1/webapp/eyeSwiftAdmins/${SystemAdmin.profile.UserID}/sites/getAllSites`,
                requestconfig
            );

    },
    
    
    AddNewCustomer: async (customer) => {
        if (!authenticationService.currentUserValue)
            return;
        let SystemAdmin = authenticationService.currentUserValue;

            let requestconfig = {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': authenticationService.currentUserValue.token,
                },
            };
            return (await axios.post(
                `${Config.globalUrl}/api/v1/webapp/eyeSwiftAdmins/${SystemAdmin.profile.UserID}/customer/signup`,
                customer,
                requestconfig
            ));
    },
    EditCustomer: async (customer_data,CustomerID) => {
        if (!authenticationService.currentUserValue)
            return;
        let SystemAdmin = authenticationService.currentUserValue;

            let requestconfig = {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': authenticationService.currentUserValue.token,
                },
            };
            return (await axios.post(
                `${Config.globalUrl}/api/v1/webapp/eyeSwiftAdmins/${SystemAdmin.profile.UserID}/customers/${CustomerID}/profile`,
                customer_data,
                requestconfig
            ));
    },
    getAllCustomers: async () => {
        if (!authenticationService.currentUserValue)
            return;
        let SystemAdmin = authenticationService.currentUserValue;

            let requestconfig = {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': authenticationService.currentUserValue.token,
                },
            };
            return await axios.get(
                `${Config.globalUrl}/api/v1/webapp/eyeSwiftAdmins/${SystemAdmin.profile.UserID}/customers/getAllCustomers`,
                requestconfig
            );

    },
    deleteCustomer: async (Customer_ID) => {
        if (!authenticationService.currentUserValue)
            return;
        let SystemAdmin = authenticationService.currentUserValue;

            let requestconfig = {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': authenticationService.currentUserValue.token,
                },
            };
            return (await axios.delete(
                `${Config.globalUrl}/api/v1/webapp/eyeSwiftAdmins/${SystemAdmin.profile.UserID}/customers/${Customer_ID}/profile`,
                requestconfig
            ));
    },

    deleteSite: async (Site_ID) => {
        if (!authenticationService.currentUserValue)
            return;
        let SystemAdmin = authenticationService.currentUserValue;

            let requestconfig = {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': authenticationService.currentUserValue.token,
                },
            };
            return (await axios.delete(
                `${Config.globalUrl}/api/v1/webapp/eyeSwiftAdmins/${SystemAdmin.profile.UserID}/sites/${Site_ID}/profile`,
                requestconfig
            ));
    },


    AddNewECP: async (ecp) => {
        if (!authenticationService.currentUserValue)
            return;
        let SystemAdmin = authenticationService.currentUserValue;

            let requestconfig = {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': authenticationService.currentUserValue.token,
                },
            };
            return (await axios.post(
                `${Config.globalUrl}/api/v1/webapp/eyeSwiftAdmins/${SystemAdmin.profile.UserID}/doctors/signup`,
                ecp,
                requestconfig
            ));
    },
    EditECP: async (ecp_data,ECP_UserID) => {
        if (!authenticationService.currentUserValue)
            return;
        let SystemAdmin = authenticationService.currentUserValue;

            let requestconfig = {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': authenticationService.currentUserValue.token,
                },
            };
            return (await axios.post(
                `${Config.globalUrl}/api/v1/webapp/eyeSwiftAdmins/${SystemAdmin.profile.UserID}/doctors/${ECP_UserID}/profile`,
                ecp_data,
                requestconfig
            ));
    },
    getAllECPs: async () => {
        if (!authenticationService.currentUserValue)
            return;
        let SystemAdmin = authenticationService.currentUserValue;

            let requestconfig = {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': authenticationService.currentUserValue.token,
                },
            };
            return await axios.get(
                `${Config.globalUrl}/api/v1/webapp/eyeSwiftAdmins/${SystemAdmin.profile.UserID}/doctors/getAllECP`,
                requestconfig
            );

    },
    AddNewAdmin: async (admin) => {
        if (!authenticationService.currentUserValue)
            return;
        let SystemAdmin = authenticationService.currentUserValue;

        let requestconfig = {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': authenticationService.currentUserValue.token,
            },
        };
        return await axios.post(
            `${Config.globalUrl}/api/v1/webapp/eyeSwiftAdmins/${SystemAdmin.profile.UserID}/admins/signup`,
            admin,
            requestconfig)

    },
    EditAdmin: async (admin_data,Admin_UserID) => {
        if (!authenticationService.currentUserValue)
            return;
        let SystemAdmin = authenticationService.currentUserValue;

            let requestconfig = {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': authenticationService.currentUserValue.token,
                },
            };
            return (await axios.post(
                `${Config.globalUrl}/api/v1/webapp/eyeSwiftAdmins/${SystemAdmin.profile.UserID}/admins/${Admin_UserID}/profile`,
                admin_data,
                requestconfig
            ));
    },
    deleteAdmin: async (Admin_UserID) => {
        if (!authenticationService.currentUserValue)
            return;
        let SystemAdmin = authenticationService.currentUserValue;

            let requestconfig = {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': authenticationService.currentUserValue.token,
                },
            };
            return (await axios.delete(
                `${Config.globalUrl}/api/v1/webapp/eyeSwiftAdmins/${SystemAdmin.profile.UserID}/admins/${Admin_UserID}/profile`,
                requestconfig
            ));
    },
    deleteEcp: async (Ecp_UserID) => {
        if (!authenticationService.currentUserValue)
            return;
        let SystemAdmin = authenticationService.currentUserValue;

            let requestconfig = {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': authenticationService.currentUserValue.token,
                },
            };
            return (await axios.delete(
                `${Config.globalUrl}/api/v1/webapp/eyeSwiftAdmins/${SystemAdmin.profile.UserID}/doctors/${Ecp_UserID}/profile`,
                requestconfig
            ));
    },
    getAllAdmins: async () => {
        if (!authenticationService.currentUserValue)
            return;
        let SystemAdmin = authenticationService.currentUserValue;

            let requestconfig = {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': authenticationService.currentUserValue.token,
                },
            };
            return await axios.get(
                `${Config.globalUrl}/api/v1/webapp/eyeSwiftAdmins/${SystemAdmin.profile.UserID}/admins/getAllAdmins`,
                requestconfig
            );

    },
    getAllDevices: async () => {
        if (!authenticationService.currentUserValue)
            return;
        let SystemAdmin = authenticationService.currentUserValue;

            let requestconfig = {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': authenticationService.currentUserValue.token,
                },
            };
            return await axios.get(
                `${Config.globalUrl}/api/v1/webapp/eyeSwiftAdmins/${SystemAdmin.profile.UserID}/devices/getAllDevices`,
                requestconfig
            );

    },
    AddNewVersion: async (version) => {
        if (!authenticationService.currentUserValue)
            return;
        let SystemAdmin = authenticationService.currentUserValue;

            let requestconfig = {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': authenticationService.currentUserValue.token,
                },
            };
            return (await axios.post(
                `${Config.globalUrl}/api/v1/webapp/eyeSwiftAdmins/${SystemAdmin.profile.UserID}/versions/addNewVersion`,
                version,
                requestconfig
            ));
    },
    editVersion: async (version_data) => {
        if (!authenticationService.currentUserValue)
            return;
        let SystemAdmin = authenticationService.currentUserValue;

            let requestconfig = {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': authenticationService.currentUserValue.token,
                },
            };
            return (await axios.post(
                `${Config.globalUrl}/api/v1/webapp/eyeSwiftAdmins/${SystemAdmin.profile.UserID}/versions/editVersion`,
                version_data,
                requestconfig
            ));
    },
    getAllVersions: async () => {
        if (!authenticationService.currentUserValue)
            return;
        let SystemAdmin = authenticationService.currentUserValue;

            let requestconfig = {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': authenticationService.currentUserValue.token,
                },
            };
            return await axios.get(
                `${Config.globalUrl}/api/v1/webapp/eyeSwiftAdmins/${SystemAdmin.profile.UserID}/versions/getAllVersions`,
                requestconfig
            );

    },
    deleteVersion: async (VersionID) => {
        if (!authenticationService.currentUserValue)
            return;
        let SystemAdmin = authenticationService.currentUserValue;

            let requestconfig = {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': authenticationService.currentUserValue.token,
                },
            };
            return (await axios.delete(
                `${Config.globalUrl}/api/v1/webapp/eyeSwiftAdmins/${SystemAdmin.profile.UserID}/versions/${VersionID}`,
                requestconfig
            ));
    },
    sendMessageToPatients: async (data) => {
        if (!authenticationService.currentUserValue)
            return;
        let SystemAdmin = authenticationService.currentUserValue;

            let requestconfig = {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': authenticationService.currentUserValue.token,
                },
            };
            return (await axios.post(
                `${Config.globalUrl}/api/v1/webapp/eyeSwiftAdmins/${SystemAdmin.profile.UserID}/patients/sendMessages`,
                data,
                requestconfig
            ));
    },
    sendMessageToEcp: async (data) => {
        if (!authenticationService.currentUserValue)
            return;
        let SystemAdmin = authenticationService.currentUserValue;

            let requestconfig = {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': authenticationService.currentUserValue.token,
                },
            };
            return (await axios.post(
                `${Config.globalUrl}/api/v1/webapp/eyeSwiftAdmins/${SystemAdmin.profile.UserID}/doctors/sendMessages`,
                data,
                requestconfig
            ));
    },
}

export default adminApi;
