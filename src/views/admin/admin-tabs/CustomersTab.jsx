import React, {Component} from 'react'
import {connect} from "react-redux";
import {cleanCustomer,setCustomerDataSelection,editCustomer,filterCustomersTable,deleteCustomer,setCustomerLockedStatus} from "../../../redux/actions/Tabs-Actions";
import MyTable from "../../../components/NovaTable/Table";
import CustomerEditForm from "../../../components/customer/CustomerEditForm"; 
import {compose} from "redux";
import {withTranslation} from "react-i18next";
import AlertConfirm from "../../../components/modals/Confirm";
import {adminApi} from "../../../services/ApiService";
import SvGraphics from "../../../assets/SvGraphics";
import Constants from "../../../utils/constants";




class CustomersTabConComposed extends Component {

    constructor(props) {
        super(props);
        this.state = {
            selected:null,
            isSideBarOpen: false,
            entry: null,
        }
    }

    async openSideBar(customer){
        await this.setState({entry:customer,isSideBarOpen:true})
        document.body.style.overflow = "hidden"
        this.forceUpdate();
    }

    componentWillUnmount(): void {
        this.props.cleanCustomer()
    }

    onSelectionChange = (selected) => {
        this.setState({selected})
        this.props.setCustomerDataSelection(selected)
    }

    editCustomerDetails = (customer) => {
        this.openSideBar(customer)

    }

    removeCustomer = async (row) => {
        let {t} = this.props
        let confirm = await AlertConfirm({
            options:{
                title: t('admin.customer.table.delete_customer'),
            }},`${t('admin.customer.table.delete_customer_confirm')}: ${row.CustomerName}?`)
        if (confirm) {
            let CustomerID = row.CustomerID
            let response = await adminApi.deleteCustomer(CustomerID)
            if (response){
                await this.props.deleteCustomer(CustomerID)
            }
        }
    }

    lockCustomer = async (row) => {
        let {t} = this.props
        let confirm = await AlertConfirm({
            options:{
                title: t('admin.customer.table.lock_customer'),
            }},`${t('admin.customer.table.lock_customer_confirm')}: ${row.CustomerName}?`)
        if (confirm) {
            let data = {'Enabled':0}
            let response = await adminApi.EditCustomer(data,row.CustomerID)
            if (response){
                await this.props.setCustomerLockedStatus(response.data)
            }
        }
    }

    unlockCustomer = async (row) => {
        let {t} = this.props
        let confirm = await AlertConfirm({
            options:{
                title: t('admin.customer.table.unlock_customer'),
            }},`${t('admin.customer.table.unlock_customer_confirm')}: ${row.CustomerName}?`)
        if (confirm) {
            let data = {'Enabled':1}
            let response = await adminApi.EditCustomer(data,row.CustomerID)
            if (response){
                await this.props.setCustomerLockedStatus(response.data)
            }
        }
    }


    setCustomerMoreFunctions = () => {
        let {t} = this.props
        return [
            {
                name: 'Edit Customer Details',
                display: t('admin.customer.table.edit_customer_details'),
                call: (customer => this.editCustomerDetails(customer))
            },
            {
                name: 'Remove Customer',
                display:t('admin.customer.table.delete_customer'),
                call: (row => this.removeCustomer(row))
            },
            {
                name: 'Lock Customer',
                display: t('admin.customer.table.lock_customer'),
                call: (row => this.lockCustomer(row))
            },
            {
                name: 'Unlock Admin',
                display: t('admin.customer.table.unlock_customer'),
                call: (row => this.unlockCustomer(row))
            }
        ]
    }

    onFilterDataSelected = (filter) => {
        this.props.filterCustomersTable(Object.keys(filter)[0],Object.values(filter)[0])
    }
    
    getColumns = (t) => {
        return [
            {accessor: 'CustomerName', Header: t('admin.customer.table.customer_name'), resizable:false}, //WAS BEFORE accessor: SiteName
            {accessor: 'CRMLink', Header: t('admin.customer.table.crm_link'), resizable:false}, //Notice the old accessors names
            //{accessor: 'LinkedSites', Header: t('admin.customer.table.linked_sites'), resizable:false},
            //{accessor: 'LinkedECPs', Header: t('admin.customer.table.linked_ecps'), resizable:false},
            //{accessor: 'LinkedDevices', Header: t('admin.customer.table.linked_devices'), resizable:false},
            {accessor: 'Enabled', Header: t('admin.customer.table.locked'), resizable: false,Cell: ({ original }) => {
                    if (original.Enabled){
                        return ''
                    } return (<SvGraphics svgname={'true'} style={{height:'15px',width:'15px', marginLeft: '19px'}}/>)
                }, makeFilter: true, filterConverter: Constants.locked},
        ]
    }

    render() {
        let {t} = this.props
        let columns = this.getColumns(t)
        let {entry,isSideBarOpen} = this.state
        return (
            <div className={'context-area'}>
                <MyTable
                    data={this.props.customerData}
                    columns={columns}
                    initialData={this.props.initiateCustomerData}
                    onSelectionChange={selected => this.onSelectionChange(selected)}
                    moreFunctions={this.setCustomerMoreFunctions()}
                    onFilterDataSelected={filter => this.onFilterDataSelected(filter)}
                    hasSelection={true}
                    hasMore={true}
                    hasShowInfo={false}
                />
                {isSideBarOpen && <CustomerEditForm
                    t={e => t(e)}
                    title={t('admin.customer.form.edit_customer')}
                    entry={entry}
                    editCustomer={customer => this.props.editCustomer(customer)}
                    isSideBarOpen={isSideBarOpen}
                    closeSideBar={() => this.setState({isSideBarOpen: false})}/>}
            </div>

        )
    }
}

function mapDispatchToProps(dispatch) {
    return {
        editCustomer: customerData => dispatch(editCustomer(customerData)),
        cleanCustomer: () => dispatch(cleanCustomer()),
        setCustomerDataSelection: customerData => dispatch(setCustomerDataSelection(customerData)),
        filterCustomersTable: (type,payload) => dispatch(filterCustomersTable(type,payload)),
        deleteCustomer: customerId => dispatch(deleteCustomer(customerId)),
        setCustomerLockedStatus: customerData => dispatch(setCustomerLockedStatus(customerData)),
    };
}

const mapStateToProps = state => {
    return {
        customerData:state.customerTableDataReducer.customerData, /////
        initiateCustomerData:state.customerTableDataReducer.initiateCustomerData ////
    };
};


const CustomersTab = compose(
    withTranslation(),
    connect(
        mapStateToProps,
        mapDispatchToProps
    )
)(CustomersTabConComposed);

export default CustomersTab;
