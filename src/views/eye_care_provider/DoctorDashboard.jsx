import React, {Component} from 'react'
import {history} from "../../utils/history";
import DoctorTabs from "./doctor-tabs/DoctorTabs";

import '../../style/NovaInfo.css'
import '../../style/rtl_css/NovaInfo.css'
import {connect} from "react-redux";
import {
    filterPatientsTable
} from "../../redux/actions/Tabs-Actions";
import {compose} from "redux";
import {withTranslation} from "react-i18next";

class DoctorDashboardConComposed extends Component {

    constructor(props) {
        super(props);
        this.state = {
            low: false,
            medium: false,
            high: false,
            total: false,
            pending: false,
            active: false,
            lock: false
        }
    }

    componentWillMount = async () : void => {


        let navTabTo = !!history.location.state && !!history.location.state.navTabTo? history.location.state.navTabTo: 0
        let pathname = (history.location.pathname).includes('/dashboard/')? history.location.pathname: `/dashboard/patients`
        await history.push({
            pathname,
            state: {
                navTabTo,
                from: {
                    fromPath: `/dashboard`,
                }
            },
        })
        this.setState({total:true})
    }

    async toggle(state) {
        let low,medium,high,total,pending,active,lock = false;
        switch (state) {
            case 'low': {low = true; break;}
            case 'medium': {medium = true; break;}
            case 'high': {high = true; break;}

            case 'total': {total = true; break;}
            case 'pending': {pending = true; break;}
            case 'active': {active = true; break;}
            case 'lock': {lock = true; break;}
            default:  break;
        }
        this.setState({
            low,medium,high,total,pending,active,lock
        })
        this.props.filterPatientsTable('status',state)
    }

    render() {
        let {low,medium,total,pending,active} = this.state
        let activeClass = 'nova-info-active-block'
        let {t} = this.props
        return (
            <div className={'nova-ecp-dashboard'}>
                <div className={'nova-info'}>
                    <div className={'nova-info-main-container container'}>
                        <div className={'nova-info-container nova-info-patients-status-container v-centered'}>
                            <label className={'nova-info-label nova-info-patients-status-label'}>{t('ecp.info_filters.patients')}:</label>
                            <div className={'nova-info-patients-status-info'}>
                                <div className={`single-nova-info-block nova-info-patients-status-red ${low?activeClass:''}`} onClick={() => this.toggle('low')}>
                                    <label className={'single-nova-info-block-value red'}>0</label>
                                    <label className={'single-nova-info-block-label'}>{t('ecp.info_filters.total_tested')}</label>
                                </div>
                                <div className={`single-nova-info-block nova-info-patients-status-yello ${medium?activeClass:''}`} onClick={() => this.toggle('medium')}>
                                    <label className={'single-nova-info-block-value yellow-dark'}>0</label>
                                    <label className={'single-nova-info-block-label'}>{t('ecp.info_filters.tested_this_week')}</label>
                                </div>
                            </div>
                        </div>
                        <div className={'nova-info-container nova-info-accounts-status-container v-centered'}>
                            <label className={'nova-info-label nova-info-accounts-status-label'}>{t('ecp.info_filters.tests')}:</label>
                            <div className={'nova-info-accounts-status-info'}>
                                <div className={`single-nova-info-block nova-info-accounts-status-total ${total?activeClass:''}`} onClick={() => this.toggle('total')}>
                                    <label className={'single-nova-info-block-value'}>{this.props.patientsCount}</label>
                                    <label className={'single-nova-info-block-label'}>{t('ecp.info_filters.avrg_num_d_tests')}</label>
                                </div>
                                <div className={`single-nova-info-block nova-info-accounts-status-active ${active?activeClass:''}`} onClick={() => this.toggle('active')}>
                                    <label className={'single-nova-info-block-value'}>{this.props.activePatientsCount}</label>
                                    <label className={'single-nova-info-block-label'}>{t('ecp.info_filters.act_dev_histo')}</label>
                                </div>
                                <div className={`single-nova-info-block nova-info-accounts-status-pending ${pending?activeClass:''}`} onClick={() => this.toggle('pending')}>
                                    <label className={'single-nova-info-block-value'}>{this.props.pendingPatientsCount}</label>
                                    <label className={'single-nova-info-block-label'}>{t('ecp.info_filters.con_bp_histo')}</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className={'nova-ecp-dashboard container'}>
                    <DoctorTabs toggle={state => this.toggle(state)}/>
                </div>
            </div>
        )
    }
}

function mapDispatchToProps(dispatch) {
    return {
        filterPatientsTable:(filterType,payload) => dispatch(filterPatientsTable(filterType,payload)),
    };
}
const mapStateToProps = state => {
    return {
        patientsData:state.patientsTableDataReducer.patientsData,
        patientsCount:state.patientsTableDataReducer.patientsCount,
        activePatientsCount:state.patientsTableDataReducer.activePatientsCount,
        pendingPatientsCount:state.patientsTableDataReducer.pendingPatientsCount,
        lockedPatientsCount:state.patientsTableDataReducer.lockedPatientsCount,
    };
};

const DoctorDashboard = compose(
    withTranslation(),
    connect(
        mapStateToProps,
        mapDispatchToProps
    )
)(DoctorDashboardConComposed)

export default DoctorDashboard;
