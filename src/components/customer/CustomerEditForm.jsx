import React, {Component} from "react";
import PropTypes from "prop-types";
import SimpleFormBuilder from "../forms_builder/SimpleFormBuilder";
import utils from "../../utils/utils";
import Prototypes from "prop-types";
import {adminApi} from "../../services/ApiService";
import * as Yup from "yup";
import validationHelper from "../../utils/validationHelper";
import {makeError} from "../modals/ErrorModal";
import Constants from "../../utils/constants";
import AlertConfirm from "../modals/Confirm";
import SvGraphics from "../../assets/SvGraphics";

class CustomerEditForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            entry: {},
            matrix :[1,1],
            errors: {},
            errorsOnSave:{},
        };
    }

    initValidation = async () => {
        let {t} = this.props
        let validationSchema = Yup.object().shape({
            CustomerName:Yup.string().required(t('admin.customer.form.customer_name_req')),
            CRMLink:Yup.string().required(t('admin.customer.form.customer_crm_req')), 
            // SiteEmail:Yup.string().matches(Constants.emailRegExp, t('admin.site.form.site_email_invalid')).required(t('admin.site.form.site_email_req')),
            // SiteCountry:Yup.string().required(t('admin.site.form.country_req')),
            // SiteState: Yup.string().when("SiteCountry", {
            //     is: value => {
            //         let us = ["us","u.s","u.s.","usa","u.s.a","u.s.a.","united states","united states of america"]
            //         return us.includes(String(value).toLowerCase())
            //     },
            //     then: Yup.string().required(
            //         t('admin.site.form.us_state_req')
            //     ).nullable(),
            //     otherwise: Yup.string().nullable()
            // })
        })
        let errors = await validationHelper.validate(this.state.entry,validationSchema)
        await this.setState({
            validationSchema,
            errors
        })
    }

    async componentWillMount(): void {
        if (this.props && this.props.entry) await this.setState({entry:utils.removeEmptyOrNull(this.props.entry)})
        await this.initValidation()
    }

    async closeSideBar(){
        let {t} = this.props
        document.body.style.overflow = "auto"
        let confirm = await AlertConfirm({
            options:{
                title: t('admin.customer.form.withdraw_edit_customer'),
            }},t('admin.customer.form.withdraw_edit_customer_info'))
        if (confirm) {
            document.body.style.overflow = "auto"
            this.props.closeSideBar(false)
            this.setState({entry:null})
        }  else {
            document.body.style.overflow = "hidden"
        }
    }

    async saveInfo(){
        let {t} = this.props
        if (!utils.isEmpty(this.state.errors)){
            this.setState({errorsOnSave:this.state.errors});
            await makeError({
                proceedLabel:  t('admin.customer.form.ok'),
                options:{
                    title: t('admin.customer.form.customer_not_edited'),
                }
            },[t('admin.customer.form.fill_all_req')]);
            return;
        }
        document.body.style.overflow = "auto"
        let confirm = await AlertConfirm({
            options:{
                title: t('admin.customer.form.edit_customer'),
            }},t('admin.customer.form.confirm_edit_customer'))
        if (confirm) {
            let {entry} = this.state
            entry = utils.ObjectEmptiesToNulls(entry)
            let response = await adminApi.EditCustomer(entry,entry.id)
            if (response) {
                document.body.style.overflow = "auto"
                this.props.closeSideBar(false)
                this.props.editCustomer(entry)
                this.setState({entry:null})
            }
        } else {
            document.body.style.overflow = "hidden"
        }
    }

    onEntryChange = async event => {
        let {entry,validationSchema} = this.state
        let name = event.target.name
        let value = event.target.value
        utils.set(entry,name,value)
        let entryValidator = utils.removeEmptyOrNull(entry)
        let errors = await validationHelper.validate(entryValidator, validationSchema);
        let errorsOnSave = this.state.errorsOnSave;
        if (utils.get(errorsOnSave,name)){
            utils.set(errorsOnSave,name,utils.get(errors,name))
        }
        this.setState({
            entry,
            errors,
            errorsOnSave
        })
        //this.props.onEntryChange(entry)
    }

    getFields = () => {
        let {t} = this.props
        return [
            {type:'text', max: 50, name:'CustomerName',display:t('admin.customer.form.customer_name'), placeholder: t('admin.customer.form.enter_customer_name'), required:true, width:0.5, style:'inline'},
            {type:'text', max: 50, name:'CRMLink',display:t('admin.customer.form.customer_crm'), placeholder: t('admin.customer.form.enter_crm'), required:true, width:0.5, style:'inline'},
            {type:'default', name:'none', placeholder: '', display:'',required:false, width:0.4, style:'inline'}
        ]
    }

    render() {
        let display = this.props.isSideBarOpen ? 'block' : 'none'
        let width = this.props.isSideBarOpen ? "600px" : "0px"
        let container_width = this.props.isSideBarOpen ? "100%" : "0px"
        let {errorsOnSave} = this.state
        let {t} = this.props

        return (
            <div className={"sidenav-complete"} style={{width:container_width, height:"100%"}}>
                <div className={"sidenav"} style={{width:width, height:"100%", float:'right'}}>
                    <SvGraphics onClick={this.closeSideBar.bind(this)} className={'close-bar-x'} svgname={'close'} height={'15px'} width={'15px'} style={{
                        float: 'left',
                        top: '10px',
                        left: '10px',
                        position: 'absolute',
                        cursor: 'pointer'
                    }}/>
                    <div className={'register-form-title container block'}>
                        <h3 className={'h-centered block'}>{this.props.title}</h3>
                    </div>
                    <div className={'register-form container block'}>
                        <SimpleFormBuilder
                            errors={errorsOnSave}
                            entry={this.state.entry}
                            matrix={this.state.matrix}
                            fields={this.getFields()}
                            onEntryChange={e => this.onEntryChange(e)}
                        />
                    </div>
                </div>
                <div className={'register-form-nav-container block'} >
                    <div className={'register-form-nav'}  style={{width:width}}>
                        <label onClick={this.closeSideBar.bind(this)} style={{display:display}} className={"v-centered close-bar"}>
                            {t('admin.customer.form.close')}
                        </label>
                        <button type={"button"} onClick={this.saveInfo.bind(this)} style={{display:display}} className={"btn btn-primary v-centered save-info"}>
                            {t('admin.customer.form.save')}
                        </button>
                    </div>
                </div>
            </div>
        );
    }
}
CustomerEditForm.propTypes = {
    isSideBarOpen:Prototypes.bool.isRequired,
    closeSideBar:Prototypes.func.isRequired,
    title: Prototypes.string.isRequired,
    editCustomer:PropTypes.func.isRequired,
    entry: Prototypes.object
}

export default CustomerEditForm

