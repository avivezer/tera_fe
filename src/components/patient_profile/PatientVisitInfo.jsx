import React, {Component} from 'react'
import '../../components_style/PatientScreen.css'
import '../../components_style/rtl_css/PatientScreen.css'
import ConstantsUtils from "../../utils/ConstantsUtils";
import utils from "../../utils/utils";


class PatientVisitInfo extends Component {

    getTreatmentProgress = (TreatmentDuration,DateOfActivation) => {
        if (DateOfActivation !== null && DateOfActivation !== undefined){
            let total = (TreatmentDuration*30).toString()
            let TreatmentStart = new Date(DateOfActivation);
            let today = new Date();
            const oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
            const diffDays = Math.round(Math.abs((today - TreatmentStart) / oneDay)).toString();
            return diffDays + '/' + total + ' Days'
        } else {
            if (TreatmentDuration !== null && TreatmentDuration !== undefined){
                let total = (TreatmentDuration*30).toString()
                return '0/' + total + ' Days'
            } else return ""
        }
    }

    render() {

        let lastVisit = this.props.lastVisit ? this.props.lastVisit : {}
        let DistanceVAUnitLabel = lastVisit ?  ConstantsUtils.getVaConstantLabel(lastVisit['DistanceVAUnit']): undefined
        let DistanceVAUnit = lastVisit ?  lastVisit['DistanceVAUnit']: undefined
        let AmblyopicEye = lastVisit ?  ConstantsUtils.getAmblyopicEye(lastVisit['AmblyopicEye']): undefined

        let RightEyeDistanceVA = DistanceVAUnit ? ConstantsUtils.convertFromLogMAR(DistanceVAUnit, lastVisit['RightEyeDistanceVA']).label : undefined
        let LeftEyeDistanceVA = DistanceVAUnit ? ConstantsUtils.convertFromLogMAR(DistanceVAUnit, lastVisit['LeftEyeDistanceVA']).label : undefined

        let WeeklyNumberOfSessions = lastVisit ?  lastVisit['WeeklyNumberOfSessions']: undefined
        let RecommendedSessionTime = lastVisit ?  lastVisit['RecommendedSessionTime']: undefined

        let TreatmentDuration  =  lastVisit ?  lastVisit['TreatmentDuration']: undefined
        let DateOfActivation  = this.props.DateOfActivation ? this.props.DateOfActivation : undefined

        let TreatmentProgressLabel = this.getTreatmentProgress(TreatmentDuration,DateOfActivation)
        return (
            <div className={'nova-patient-right-left inline'}>
                <div className={'nova-patient-right-left-element nova-patient-right-left-bottom'}>
                    <div className={'nova-patient-right-left-bottom-element v-centered inline'}>
                        <div className={'nova-patient-right-left-bottom-left-element '}>
                            <label className={'patient-value h-centered'}>{
                                TreatmentProgressLabel
                            }</label>
                        </div>
                        <label className={'patient-page-value-label h-centered'}>
                            Number of tests run
                        </label>
                    </div>
                    <div className={'nova-patient-right-left-bottom-element v-centered inline'}>
                        <div className={'nova-patient-right-left-bottom-left-element'}>
                            <label className={'patient-value h-centered'}>{
                                utils.time_convert(RecommendedSessionTime)
                            }</label>
                        </div>
                        <label className={'patient-page-value-label h-centered'}>
                            Total tests run
                        </label>
                    </div>
                    <div className={'nova-patient-right-left-bottom-element v-centered inline'}>
                        <div className={'nova-patient-right-left-bottom-left-element'}>
                            <label className={'patient-value h-centered'}>{
                                WeeklyNumberOfSessions
                            }</label>
                        </div>
                        <label className={'patient-page-value-label h-centered'}>
                            Total visits
                        </label>
                    </div>
                </div>
            </div>
        )
    }
}

export default PatientVisitInfo;
