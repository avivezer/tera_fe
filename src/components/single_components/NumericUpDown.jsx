import React, {Component} from 'react'
import PropTypes from "prop-types";
import "../../components_style/NumericUpDown.css";
import '../../components_style/rtl_css/NumericUpDown.css'
import utils from "../../utils/utils";

class NumericUpDown extends Component {
    constructor(props) {
        super(props);

        this.state = {
            minValue: 45,
            value: 90,
        }
    }

    componentWillMount() {
        if (this.props.value) {
            let value = (Number(this.props.value)) - 15 >= this.state.minValue ?
                (Number(this.props.value)) :
                (Number(this.state.minValue))
            this.setState({value})
            this.props.onValueChange(this.props.name,Number(value))
        } else {
            this.props.onValueChange(this.props.name,this.state.value)
        }

    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.value !== prevProps.value) {
            this.setState({
                value: this.props.value
            })
        }
    }

    handleNumericUp = () => {
        let value = Number(this.state.value);
        value = (value + 15);
        this.setState({value: value})
        if (this.props.onValueChange) {
            this.props.onValueChange(this.props.name,value)
        }
    }

    handleNumericDown = () => {
        let value = Number(this.state.value);
        value = (value - 15);
        if (value >= this.state.minValue){
            this.setState({value: value})
            if (this.props.onValueChange) {
                this.props.onValueChange(this.props.name,value)
            }
        }
    }

    render() {


        return (
            <div className={'form-numericUpDown'}>
                <input
                    className={'nova-form-input nova-form-numericUpDown'}
                    disabled

                    value={utils.time_convert(this.state.value)}
                />
                <button className={'btn btn-primary eye-swift-numericUpDown-button eye-swift-numericUpDown-plus-form-button'}
                        disabled={this.props.disabled} onClick={this.handleNumericUp.bind(this)}>
                    <div className={'centered button-label'}>+</div>
                </button>
                <button className={'btn btn-primary eye-swift-numericUpDown-button eye-swift-numericUpDown-minus-form-button'}
                        disabled={this.props.disabled} onClick={this.handleNumericDown.bind(this)}>
                    <div className={'centered button-label minus-button'}>-</div>
                </button>
            </div>
        );
    }
}

NumericUpDown.propTypes = {
    value: PropTypes.number,
    disabled:PropTypes.bool,
    name: PropTypes.string.isRequired,
    onValueChange: PropTypes.func.isRequired,
};

export default NumericUpDown;
