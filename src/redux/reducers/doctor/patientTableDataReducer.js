import {
    CLEAN_PATIENT,
    DELETE_PATIENT,
    EDIT_PATIENT,
    FILTER_PATIENTSDATA,
    NEW_PATIENT,
    PATIENT_LOCKED_STATUS,
    PATIENTS_COUNT,
    PATIENTS_DATA,
    PATIENTS_SELECTION,
    PATIENTS_STATUSES
} from "../../constants/Tabs-Action-Types";
import utils from "../../../utils/utils";
import ConstantsUtils from "../../../utils/ConstantsUtils";


const initialState = {
    initiatePatientsData:[],
    patientsData:[],
    filterPatientsData:[],
    selectedPatientsData:[],
    patientsTableFilter:{},
    patientsCount: 0,
    activePatientsCount: 0,
    pendingPatientsCount: 0,
    lockedPatientsCount: 0
};

const getTreatmentDurationLabel = (visit,DateOfActivation) => {
    let TreatmentDuration  =  visit ?  visit['TreatmentDuration']: undefined
    if (DateOfActivation !== null && DateOfActivation !== undefined){
        let total = (TreatmentDuration*30).toString()
        let TreatmentStart = new Date(DateOfActivation);
        let today = new Date();
        const oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
        const diffDays = Math.round(Math.abs((today - TreatmentStart) / oneDay)).toString();
        return diffDays + '/' + total + ' Days'

    } else {
        let total = (TreatmentDuration*30).toString()
        return '0/' + total + ' Days'
    }

}

const getTreatmentDuration = (visit,DateOfActivation) => {
    let TreatmentDuration  =  visit ?  visit['TreatmentDuration']: undefined
    if (DateOfActivation !== null && DateOfActivation !== undefined){
        let total = TreatmentDuration*30
        let TreatmentStart = new Date(DateOfActivation);
        let today = new Date();
        const oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
        const diffDays = Math.round(Math.abs((today - TreatmentStart) / oneDay)).toString();
        return ((diffDays/total)*100).toFixed(2)
    } else {
        return 0
    }
}

const patientsTableDataReducer = (state = initialState, action)=> {

    switch(action.type){
        case PATIENTS_COUNT: {
            let _idata = JSON.parse(JSON.stringify(action.payload));
            state = {
                ...state,
                patientsCount: _idata
            };
            break;
        }
        case PATIENTS_DATA: {
            let collection = []

            let active = 0
            let pending = 0
            let locked = 0

            action.payload.forEach(patient => {
                if (patient.PII !== undefined && patient.PII !== null) {
                    if (patient.PII.User.Enabled === 0) locked++
                    if (patient.AccountStatus === 1) pending++
                    if (patient.AccountStatus === 2) active++
                    collection.push({
                        id: patient.UserID,
                        UserID: patient.UserID,
                        DoctorID: patient.DoctorID,
                        TermsOfUseSigned: patient.TermsOfUseSigned,
                        NextVisit: patient.NextVisit !== null && patient.NextVisit !== undefined ? utils.getDateFormatWithYear(new Date(patient.NextVisit)) : '',
                        Notes: patient.Notes,
                        LastNotification: patient.LastNotification,
                        FullName: patient.PII.FirstName + ' ' + patient.PII.LastName,
                        FirstName: patient.PII.FirstName,
                        LastName: patient.PII.LastName,
                        Email: patient.PII.Email,
                        Gender: patient.PII.Gender, //missing is 0
                        Birthdate: utils.getDateFormatWithYear(new Date(patient.PII.Birthdate)),
                        SocialID: patient.PII.SocialID,
                        ID: patient.PII.SocialID,
                        Country: patient.PII.Country,
                        State: patient.PII.State,
                        City: patient.PII.City,
                        Street: patient.PII.Street,
                        Apartment: patient.PII.Apartment,
                        ZIPCode: patient.PII.ZIPCode,
                        PhoneNumber: patient.PII.PhoneNumber,
                        Role: patient.PII.User.Role[0],
                        Enabled: patient.PII.User.Enabled,
                        AccountStatus: patient.AccountStatus,
                        AccountStatusLabel: ConstantsUtils.getPatient_AccountStatus(patient.AccountStatus),
                        Status: 0
                    })
                }
            })

            let _pdata = JSON.parse(JSON.stringify(collection));
            let _aadata = JSON.parse(JSON.stringify(active));
            let _apdata = JSON.parse(JSON.stringify(pending));
            let _sldata = JSON.parse(JSON.stringify(locked));
            state = {
                ...state,
                initiatePatientsData: _pdata,
                patientsData: _pdata,
                filterPatientsData: _pdata,
                patientsCount: collection.length,
                activePatientsCount: _aadata,
                pendingPatientsCount: _apdata,
                lockedPatientsCount: _sldata
            };
            break;
        }
        case NEW_PATIENT: {
            let patient = action.payload
            let patientsData = state.patientsData
            let initiatePatientsData = state.initiatePatientsData

            let newData = [{
                id:patient.UserID,
                UserID:patient.UserID,
                DoctorID:patient.Patient.DoctorID,
                TermsOfUseSigned: patient.TermsOfUseSigned,
                NextVisit: patient.NextVisit !== null && patient.NextVisit !== undefined ?utils.getDateFormatWithYear(new Date(patient.NextVisit)):'',
                Notes: patient.Notes,
                LastNotification:patient.LastNotification,
                FullName: patient.PIIProfile.FirstName + ' ' + patient.PIIProfile.LastName,
                FirstName: patient.PIIProfile.FirstName,
                LastName: patient.PIIProfile.LastName,
                Email: patient.PIIProfile.Email,
                Gender: patient.PIIProfile.Gender,
                Birthdate: utils.getDateFormatWithYear(new Date(patient.PIIProfile.Birthdate)),
                SocialID: patient.PIIProfile.SocialID,
                ID: patient.PIIProfile.SocialID,
                Country: patient.PIIProfile.Country,
                State: patient.PIIProfile.State,
                City: patient.PIIProfile.City,
                Street: patient.PIIProfile.Street,
                Apartment: patient.PIIProfile.Apartment,
                ZIPCode: patient.PIIProfile.ZIPCode,
                PhoneNumber: patient.PIIProfile.PhoneNumber,
                Role: patient.Role[0],
                Enabled:patient.Enabled,
                AccountStatus: patient.Patient.AccountStatus,
                AccountStatusLabel: ConstantsUtils.getPatient_AccountStatus(patient.Patient.AccountStatus),
                Status:1,
            }]

            let newpatientsData = patientsData.concat(newData)
            let newinitiatePatientsData = initiatePatientsData.concat(newData)

            let _pdata = JSON.parse(JSON.stringify(newpatientsData));
            let _idata = JSON.parse(JSON.stringify(newinitiatePatientsData));
            state = {
                ...state,
                initiatePatientsData:_idata,
                patientsData : _pdata,
                patientsCount: newinitiatePatientsData.length,
                pendingPatientsCount: state.activePatientsCount+1,
            };
            break;
        }
        case CLEAN_PATIENT:{
            state = {
                ...state,
                patientsTableFilter: {},
                selectedPatientsData:[]
            };
            break;
        }
        case FILTER_PATIENTSDATA: {
            let type = action.searchType
            let payload = action.payload
            let patientsTableFilter = state.patientsTableFilter
            let filteredPatientsData = state.initiatePatientsData
            let visibleFilterData = []
            let visiableData = []
            if (type === 'status' && payload === 'total'){
                delete patientsTableFilter.status
            } else {
                utils.set(patientsTableFilter,type,payload)
            }

            for (const [key, value] of Object.entries(patientsTableFilter)) {
                if (key === 'search'){
                    let val = value.toLowerCase()
                    filteredPatientsData = filteredPatientsData.filter(x =>
                        String(x.FullName).toLowerCase().indexOf(val) > -1 ||
                        String(x.UserID).toLowerCase().indexOf(val) > -1 ||
                        String(x.SocialID).toLowerCase().indexOf(val) > -1 ||
                        String(x.AccountStatusLabel).toLowerCase().indexOf(val) > -1 ||
                        String(x.NextVisit).toLowerCase().indexOf(val) > -1
                    )
                    visibleFilterData = filteredPatientsData
                } else if (key === 'status') {

                    switch (value) {

                        case 'active':{
                            filteredPatientsData = filteredPatientsData.filter(patient => patient['AccountStatus'] === 2)
                            break;
                        }
                        case 'pending':{
                            filteredPatientsData = filteredPatientsData.filter(patient => patient['AccountStatus'] === 1)
                            break;
                        }
                        case 'lock':{
                            filteredPatientsData = filteredPatientsData.filter(patient => patient['Enabled'] === 0)
                            break;
                        }
                        default:{
                            break;
                        }
                    }
                    visibleFilterData = filteredPatientsData
                } else {
                        let patients = JSON.parse(JSON.stringify(filteredPatientsData))
                        let valArr = JSON.parse(JSON.stringify(value))


                        if (Array.isArray(valArr)){
                            let res = []
                            valArr.forEach(val => {
                                let patientArr = patients.filter(patient => {
                                    let patientVal = patient[key]
                                    return String(patientVal).toLowerCase() === String(val).toLowerCase()
                                })
                                patientArr.forEach(patient => res.push(patient))
                            })
                            filteredPatientsData = res
                        }
                }
            }
            visiableData = filteredPatientsData

            let _fpdata = JSON.parse(JSON.stringify(visibleFilterData));
            let _vdata = JSON.parse(JSON.stringify(visiableData));
            let _pfdata = JSON.parse(JSON.stringify(patientsTableFilter));
            state = {
                ...state,
                filterPatientsData: _fpdata,
                patientsData: _vdata,
                patientsTableFilter: _pfdata
            };
            break;
        }
        case PATIENTS_SELECTION:{
            let selection = action.payload
            let initiatePatientsData = state.initiatePatientsData

            let newSelection = []
            Object.entries(selection).forEach(([key,value]) => {
                if (value){
                    let index = initiatePatientsData.findIndex((obj => obj.UserID === key));
                    let name = initiatePatientsData[index].FullName
                    newSelection.push({id:key,name})
                }
            })
            state = {
                ...state,
                selectedPatientsData: newSelection
            };
            break;
        }
        case EDIT_PATIENT: {
            let patient = action.payload
            let patientsData = state.patientsData
            let initiatePatientsData = state.initiatePatientsData

            let locked = state.lockedPatientsCount

            let indexData = patientsData.findIndex((obj => obj.UserID === patient.UserID));
            let indexInit = initiatePatientsData.findIndex((obj => obj.UserID === patient.UserID));

            let isLocked = patientsData[indexData]['Enabled'] === 0

            Object.keys(patient).forEach(key => {
                let newVal = patient[key]
                if (patientsData[indexData][key])
                    patientsData[indexData][key] = newVal
                if (initiatePatientsData[indexInit][key])
                    initiatePatientsData[indexInit][key] = newVal
            })

            if (isLocked && patientsData[indexData]['Enabled'] === 1) locked--
            else if (!isLocked && patientsData[indexData]['Enabled'] === 0 ) locked++


            let _pdata = JSON.parse(JSON.stringify(patientsData));
            let _idata = JSON.parse(JSON.stringify(initiatePatientsData));

            let _sldata = JSON.parse(JSON.stringify(locked));

            state = {
                ...state,
                initiatePatientsData:_idata,
                patientsData : _pdata,
                filterPatientsData: _pdata,
                lockedPatientsCount: _sldata
            };
            break;
        }
        case PATIENT_LOCKED_STATUS: {
            let patient = action.payload
            let patientsData = state.patientsData
            let initiatePatientsData = state.initiatePatientsData

            let locked = state.lockedPatientsCount

            let indexData = patientsData.findIndex((obj => obj.UserID === patient.UserID));
            let indexInit = initiatePatientsData.findIndex((obj => obj.UserID === patient.UserID));

            let isLocked = patientsData[indexData]['Enabled'] === 0

            let Enabled = patient.PII.User.Enabled

            patientsData[indexData]['Enabled'] = Enabled
            initiatePatientsData[indexInit]['Enabled'] = Enabled

            if (isLocked && patientsData[indexData]['Enabled'] === 1) locked--
            else if (!isLocked && patientsData[indexData]['Enabled'] === 0 ) locked++

            let _pdata = JSON.parse(JSON.stringify(patientsData));
            let _idata = JSON.parse(JSON.stringify(initiatePatientsData));

            let _sldata = JSON.parse(JSON.stringify(locked));
            state = {
                ...state,
                initiatePatientsData:_idata,
                patientsData : _pdata,
                filterPatientsData: _pdata,
                lockedPatientsCount: _sldata
            };
            break;
        }
        case DELETE_PATIENT: {
            let patientId = action.payload
            let patientsData = state.patientsData
            let initiatePatientsData = state.initiatePatientsData
            let locked = state.lockedPatientsCount
            let active = state.activePatientsCount
            let pending = state.pendingPatientsCount

            let indexData = patientsData.findIndex((obj => obj.UserID === patientId));
            let isLocked = patientsData[indexData]['Enabled'] === 0

            let isActive = patientsData[indexData]['AccountStatus'] === 2
            let isPending = patientsData[indexData]['AccountStatus'] === 1

            let newPatientData = patientsData.filter(patient => patient.UserID !== patientId)
            let newInitiatePatientsData = initiatePatientsData.filter(patient => patient.UserID !== patientId)

            if (isLocked) locked--
            if (isActive) active--
            if (isPending) pending--

            let _pdata = JSON.parse(JSON.stringify(newPatientData));
            let _idata = JSON.parse(JSON.stringify(newInitiatePatientsData));

            let _aadata = JSON.parse(JSON.stringify(active));
            let _apdata = JSON.parse(JSON.stringify(pending));
            let _sldata = JSON.parse(JSON.stringify(locked));
            state = {
                ...state,
                initiatePatientsData:_idata,
                patientsData : _pdata,
                filterPatientsData: _pdata,
                patientsCount: newInitiatePatientsData.length,
                activePatientsCount: _aadata,
                pendingPatientsCount: _apdata,
                lockedPatientsCount: _sldata
            };
            break;
        }
        case PATIENTS_STATUSES: {

            break;
        }
        default:
            break;
    }
    return state;
};
export default patientsTableDataReducer;
