import {
    CLEAN_CUSTOMER,
    EDIT_CUSTOMER,
    FILTER_CUSTOMERSDATA,
    NEW_CUSTOMER,
    CUSTOMERS_COUNT,
    CUSTOMERS_DATA,
    CUSTOMERS_SELECTION,
    DELETE_CUSTOMER,
    CUSTOMER_LOCKED_STATUS
} from "../../constants/Tabs-Action-Types";
import utils from "../../../utils/utils";


const initialState = {
    initiateCustomerData:[],
    customerData:[],
    customerTableFilter:{},
    customersCount: 0
};

function calculateDevices(customer) { //IS NEVER USED
    let ecps = customer.EyeCareProviders?customer.EyeCareProviders.length:0
    if (ecps === 0) return 0

    let numberOfDevices = 0
    customer.EyeCareProviders.forEach(ecp => {
        if (ecp.Patients)
        numberOfDevices += ecp.Patients.length
    })
    return numberOfDevices
}

const customerTableDataReducer = (state = initialState, action)=> {

    switch(action.type){
        case CUSTOMERS_COUNT:{
            let _idata = JSON.parse(JSON.stringify(action.payload));
            state = {
                ...state,
               customersCount: _idata
            };
            break;
        }
        case CUSTOMERS_DATA:
            let collection = []
            action.payload.forEach(customer =>{
                collection.push({
                    id:customer.CustomerID,
                    CustomerID: customer.CustomerID,
                    CustomerName: customer.CustomerName,
                    CRMLink: customer.CRMLink,
                    //LinkedSites: customer.NumOfSites,
                    //LinkedECPs: customer.NumOfECP,
                    //LinkedDevices: customer.NumOfDevices,
                    Enabled: customer.Enabled,
                    AccountStatus: customer.AccountStatus,
                })
            })

            let _pdata = JSON.parse(JSON.stringify(collection));
            state = {
                ...state,
                initiateCustomerData:_pdata,
                customerData : _pdata,
                customersCount: collection.length
            };
            break;
        case CLEAN_CUSTOMER:{
            state = {
                ...state,
                customerTableFilter: {}
            };
            break;
        }
        case FILTER_CUSTOMERSDATA:{
            let type = action.searchType
            let payload = action.payload
            let customerTableFilter = state.customerTableFilter
            let filteredCustomersData = state.initiateCustomerData
            let visibleFilterData = []
            let visiableData = []

            if (type === 'search' && (payload === '' || payload === null)){
                delete customerTableFilter.search
            } else {
                utils.set(customerTableFilter,type,payload)
            }

            for (const [key, value] of Object.entries(customerTableFilter)) {
                if (key === 'search'){
                    let val = value.toLowerCase()
                    filteredCustomersData = filteredCustomersData.filter(x =>
                        String(x.CustomerName).toLowerCase().indexOf(val) > -1 ||
                        String(x.CRMLink).toLowerCase().indexOf(val) > -1 
                        //String(x.LinkedSites).toLowerCase().indexOf(val) > -1 ||
                        //String(x.LinkedECPs).toLowerCase().indexOf(val) > -1 ||
                        //String(x.LinkedDevices).toLowerCase().indexOf(val) > -1 
                    )
                    visibleFilterData = filteredCustomersData
                } else {
                    let customers = JSON.parse(JSON.stringify(filteredCustomersData))
                    let valArr = JSON.parse(JSON.stringify(value))


                    if (Array.isArray(valArr)){
                        let res = []
                        valArr.forEach(val => {
                            let customerArr = customers.filter(customer => {
                                let customerVal = customer[key]
                                return String(customerVal).toLowerCase() === String(val).toLowerCase()
                            })
                            customerArr.forEach(customer => res.push(customer))
                        })
                        filteredCustomersData = res
                    }
                }
            }
            visiableData = filteredCustomersData

            let _fpdata = JSON.parse(JSON.stringify(visibleFilterData));
            let _vdata = JSON.parse(JSON.stringify(visiableData));
            let _pfdata = JSON.parse(JSON.stringify(customerTableFilter));
            state = {
                ...state,
                filteredCustomersData: _fpdata,
                customerData: _vdata,
                customerTableFilter: _pfdata
            };
            break;
        }
        case CUSTOMERS_SELECTION:{
            let selection = action.payload
            let initiateCustomerData = state.initiateCustomerData

            let newSelection = []
            Object.entries(selection).forEach(([key,value]) => {
                if (value){
                    let index = initiateCustomerData.findIndex((obj => obj.CustomerID === key));
                    let name = initiateCustomerData[index].CustomerName
                    newSelection.push({id:key,name})
                }
            })
            state = {
                ...state,
                selectedcustomerData: newSelection
            };
            break;
        }
        case NEW_CUSTOMER:{
            let customer = action.payload
            let customerData = state.customerData
            let initiateCustomerData = state.initiateCustomerData

            let newData = [{
                id:customer.CustomerID,
                CustomerID: customer.CustomerID,
                CustomerName: customer.CustomerName,
                CRMLink: customer.CRMLink,
                //LinkedSites: customer.Sites?customer.Sites.length:0,
                //LinkedECPs: customer.EyeCareProviders?customer.EyeCareProviders.length:0,
                //LinkedDevices: customer.LinkedDevices?customer.LinkedDevices.length:0,
                Enabled: customer.Enabled,
                AccountStatus: customer.AccountStatus,
            }]
            let newCustomerData = customerData.concat(newData)
            let newInitiateCustomerData = initiateCustomerData.concat(newData)
            let _pdata = JSON.parse(JSON.stringify(newCustomerData));
            let _idata = JSON.parse(JSON.stringify(newInitiateCustomerData));
            state = {
                ...state,
                initiateCustomerData:_idata,
                customerData : _pdata,
                customersCount: newInitiateCustomerData.length
            };
            break;
        }
        case EDIT_CUSTOMER: {
            let customer = action.payload
            let customerData = state.customerData
            let initiateCustomerData = state.initiateCustomerData

            let indexData = customerData.findIndex((obj => obj.id === customer.id));
            let indexInit = initiateCustomerData.findIndex((obj => obj.id === customer.id));

            customerData[indexData]['CustomerName'] = customer.CustomerName
            customerData[indexData]['CRMLink'] = customer.CRMLink
            
            initiateCustomerData[indexInit]['CustomerName'] = customer.CustomerName
            initiateCustomerData[indexInit]['CRMLink'] = customer.CRMLink
            

            let _pdata = JSON.parse(JSON.stringify(customerData));
            let _idata = JSON.parse(JSON.stringify(initiateCustomerData));
            state = {
                ...state,
                initiateCustomerData:_idata,
                customerData : _pdata
            };
            break;
        }

        case DELETE_CUSTOMER: {
            let customerId = action.payload
            let customerData = state.customerData
            let initiateCustomerData = state.initiateCustomerData
            let newcustomerData = customerData.filter(customer => customer.CustomerID !== customerId)
            let newinitiateCustomerData = initiateCustomerData.filter(customer => customer.CustomerID !== customerId)
            let _pdata = JSON.parse(JSON.stringify(newcustomerData));
            let _idata = JSON.parse(JSON.stringify(newinitiateCustomerData));
            state = {
                ...state,
                initiateCustomerData:_idata,
                customerData : _pdata,
                customersCount: newinitiateCustomerData.length
            };
            break;
        }

        case CUSTOMER_LOCKED_STATUS: {
            let customer = action.payload
            let customerData = state.customerData
            let initiateCustomerData = state.initiateCustomerData

            let indexData = customerData.findIndex((obj => obj.CustomerID === customer.CustomerID));
            let indexInit = initiateCustomerData.findIndex((obj => obj.CustomerID === customer.CustomerID));
            let Enabled = customer.Enabled

            customerData[indexData]['Enabled'] = Enabled
            initiateCustomerData[indexInit]['Enabled'] = Enabled

            let _pdata = JSON.parse(JSON.stringify(customerData));
            let _idata = JSON.parse(JSON.stringify(initiateCustomerData));
            state = {
                ...state,
                initiateCustomerData:_idata,
                customerData : _pdata
            };
            break;
        }
        default:
            break;
    }
    return state;
};
export default customerTableDataReducer;
